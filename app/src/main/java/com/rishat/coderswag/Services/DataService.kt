package com.rishat.coderswag.Services

import com.rishat.coderswag.Model.Category
import com.rishat.coderswag.Model.Product

object DataService {

    val categories = listOf(
        Category("BAR","bar1"),
        Category("CLUB","club"),
        Category("COFFEE","coffee1"),
        Category("RESTAURANT","restaurant"),
        Category("КАЛЬЯННАЯ","vaper")
    )

    val bar = listOf(
        Product("Бар Синий Фантомас","$1","bar1"),
        Product("Бар Чукотка","$2","bar3"),
        Product("Бар Синий Фантомас","$1","bar1"),
        Product("Бар Чукотка","$2","bar3"),
        Product("Ресторан Paradase","$5","restaurant1"),
        Product("Ресторан Ambasador","$9","restaurant2"),
        Product("Ресторан Сарай","$10","restaurant"),
        Product("Ресторан Paradase","$5","restaurant1"),
        Product("Ресторан Ambasador","$9","restaurant2")

    )

    val restaurant = listOf(
        Product("Ресторан Сарай","$10","restaurant"),
        Product("Ресторан Paradase","$5","restaurant1"),
        Product("Ресторан Ambasador","$9","restaurant2"),
        Product("Ресторан Сарай","$10","restaurant"),
        Product("Ресторан Paradase","$5","restaurant1"),
        Product("Ресторан Ambasador","$9","restaurant2"),
        Product("Ресторан Сарай","$10","restaurant"),
        Product("Ресторан Paradase","$5","restaurant1"),
        Product("Ресторан Ambasador","$9","restaurant2")
    )

    val coffee = listOf(
        Product("Coffedelia","$2","coffee1"),
        Product("Starbuks","$1","coffee2"),
        Product("Coffedelia","$2","coffee1"),
        Product("Starbuks","$1","coffee2"),
        Product("Coffedelia","$2","coffee1"),
        Product("Starbuks","$1","coffee2"),
        Product("Coffedelia","$2","coffee1"),
        Product("Starbuks","$1","coffee2")
    )

    val club = listOf(
        Product("Disco-80","$6","club")
    )

    val vaper = listOf(
        Product("VapeShop","$2","vaper"),
        Product("Бар Синий Фантомас","$1","bar1"),
        Product("Бар Чукотка","$2","bar3"),
        Product("Бар Синий Фантомас","$1","bar1"),
        Product("Бар Чукотка","$2","bar3")
    )

    val digitalGood = listOf<Product>()

    fun getProducts(category: String) : List<Product> {
        return when(category) {
            "BAR" -> bar
            "CLUB" -> restaurant
            "COFFEE" -> coffee
            "RESTAURANT" -> club
            "КАЛЬЯННАЯ" -> vaper
            else -> digitalGood

        }
    }
}